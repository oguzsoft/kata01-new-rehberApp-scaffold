## How to create a new Rails App with scaffold
Open terminal to create a Rails project

* ### Create a new rails app "kata01-new-rehberApp-scaffold"
 ```rails new kata01-new-rehberApp-scaffold -B -T```


* ### Run bundle command 
  ```bundle install --local```

* ### Run rails server to check your app
  ```rails s```

  Open your browsser and go to ```localhost:3000```, If you see "Yay you are on rails " message, no problem. You can go to the next step. 

* ### Close the rails server 
  ```cntrl + c``` 

* ### Using Scaffold
  ```rails g scaffold Person name lastname phone ```
* ### Migrating the database 
   ```rails db:migrate```


* ### Go to your ```config/routes.rb``` and add this line
  ```root 'people#index'```

* ### Run the rails server
  ```rails s```

## How to add Foundation to existing Rails project?

* Open your Gemfile file and add those lines to file. 

  ```
  gem 'foundation-rails', '6.4.3.0'
  gem 'foundation-icons-sass-rails'
  ```

* ### Run bundle command
  ```bundle install --local```


* ### Run foundation installion command 
  ```rails g foundation:install --force```

* ### Rename file. Run this command

  ```mv app/assets/stylesheets/application.css app/assets/stylesheets/application.css.scss``` 

* ### Open ```app/assets/stylesheets/application.css.scss``` file and add those lines to file. (For Motion UI)

      ```
      @import "foundation-icons";
      @import 'motion-ui/motion-ui';
      @include motion-ui-transitions;
      @include motion-ui-animations;
      ```


* ###  Install JQuery using yarn
  ```yarn add jquery```

* ### Open ```app/assets/javascripts/application.js``` Add jquery before the foundation line
  ```
    //= require rails-ujs
    //= require jquery
    //= require foundation
    //= require turbolinks
    //= require_tree .
  ```


* ### compile all assets
  ```rails assets:precompile```

  > When Restart your Rails Server and refresh your browser you will see difference


